## Book Store API

This is an API for a book store

## Steps
- The only thing you need to get it up is docker-compose up, and you can start playing.

```
docker-compose up 
```

## Tools 

- Python was used as language
- Fastapi Framework
- MongoDB
- pymongo
- pyjwt (Tokens)
- schedule
- python-dotenv
- Docker & Docker-compose 

## Documentation

- All documentation is in 
```
http://localhost:8080/docs
```
## Why:

### Fast API:
- Fast API validates the developer’s data type even in deeply nested JSON requests.
Fast API is built on standards like JSON Schema (a tool used for validating the structure of JSON data), OAuth 2.0 (it’s the industry-standard protocol for authorization), and OpenAPI (which is a publicly available application programming interface)
FastAPI makes it easy to build a GraphQL API with a Python library called graphene-python.

### MongoDB
- Flexible document schemas
- Code-native data access
- Change-friendly design
- Powerful querying and analytics
- Easy horizontal scale-out

***

# Commnets " what would I do better? "

- I would use a production rds for the database and its scalability
- due to lack of time I could not add graphane to use graphql

## Why graphql 
- GraphQL is not dealing with dedicated resources
- GraphQL API will only expose a single endpoint
- Over Fetching & Under Fetching
- A GraphQL API will only use the POST method
- WebSockets , Resolvers, error manage and the verisoning when you work with many developers, it is important

### Author 
- maprigo
